import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Friends from './components/Friends/Friends';
import Header from './components/Header/Header';
import Messages from './components/Messages/Messages';
import Music from './components/Music/Music';
import News from './components/News/News';
import Profile from './components/Profile/Profile';
import Settings from './components/Settings/Settings';
import Sidebar from './components/Sidebar/Sidebar';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Sidebar />
        
        <main>
          <Route path='/profile' component={Profile} />   
          <Route path='/messages' component={Messages} />
          <Route path='/friends' component={Friends} />
          <Route path='/music' component={Music} />
          <Route path='/news' component={News} />
          <Route path='/settings' component={Settings} />  
        </main>
      </div>
    </BrowserRouter>
  );
}

export default App;
