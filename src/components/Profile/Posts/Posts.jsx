import React from 'react';
import CreatePost from './CreatePost/CreatePost';
import Post from './Post/Post';
import style from './Posts.module.css';

const Posts = () => {
    return(
        <div className={style.posts}>
            <CreatePost />
            <Post />
            <Post />
            <Post />
        </div>
    );
};

export default Posts;