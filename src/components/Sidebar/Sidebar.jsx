import React from 'react';
import { NavLink } from 'react-router-dom';
import style from './Sidebar.module.css';

const Sidebar = () => {
    return(
        <aside className={style.sidebar}>
            <div className={style.sidebar_link}><NavLink to="/profile">Profile</NavLink></div>
            <div className={style.sidebar_link}><NavLink to="/messages">Messages</NavLink></div>
            <div className={style.sidebar_link}><NavLink to="/friends">Friends</NavLink></div>
            <div className={style.sidebar_link}><NavLink to="/music">Music</NavLink></div>
            <div className={style.sidebar_link}><NavLink to="/news">News</NavLink></div>
            <div className={style.sidebar_link}><NavLink to="/settings">Settings</NavLink></div>
        </aside>
    );
};

export default Sidebar;